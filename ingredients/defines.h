#ifndef DEFINES_H
#define DEFINES_H

#define PI 3.141592653589793
#define TWOPI 6.2831853071795862
#define TWOPI_F 6.2831853f
#define TO_RADIANS(x) (x * 0.017453292519943295)
#define TO_DEGREES(x) (x * 57.29577951308232)

// ここから値を追加
// 描画する球の細かさ
#define TESS 10
// 扱う頂点の上限
#define MAX_NUM 100
// 画面の縦横サイズ。だがここに記述されているものは飾りで、実際はmain.cppがサイズを決定している。
#define WIDTH 600
#define HEIGHT 400
// 球の半径
#define Rad (0.05 * 0.15)
#endif // DEFINES_H
