#include "vboline.h"
#include "defines.h"

#include "cookbookogl.h"
#include "glutils.h"

#include <cstdio>

VBOLine::VBOLine(float s_xpos, float s_ypos, float s_zpos, float e_xpos, float e_ypos, float e_zpos)
{
    
    float v[2*3] = {
       s_xpos, s_ypos, s_zpos,
       e_xpos, e_ypos, e_zpos
    };

    float n[2*3] = {
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f
    };

    float tex[2*2] = {
        0.0f, 0.0f,
        1.0f, 0.0f
    };

    GLuint el[] = {
        0,1
    };

    glGenVertexArrays( 1, &vaoHandle );
    glBindVertexArray(vaoHandle);

    unsigned int handle[4];
    glGenBuffers(4, handle);

    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, 2 * 3 * sizeof(float), v, GL_STATIC_DRAW);
    glVertexAttribPointer( (GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, ((GLubyte *)NULL + (0)) );
    glEnableVertexAttribArray(0);  // Vertex position

    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, 2 * 3 * sizeof(float), n, GL_STATIC_DRAW);
    glVertexAttribPointer( (GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, ((GLubyte *)NULL + (0)) );
    glEnableVertexAttribArray(1);  // Vertex normal

    glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
    glBufferData(GL_ARRAY_BUFFER, 2 * 2 * sizeof(float), tex, GL_STATIC_DRAW);
    glVertexAttribPointer( (GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, ((GLubyte *)NULL + (0)) );
    glEnableVertexAttribArray(2);  // texture coords

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2 * sizeof(GLuint), el, GL_STATIC_DRAW);

    glBindVertexArray(0);
}

void VBOLine::render() {
    glBindVertexArray(vaoHandle);
    glDrawElements(GL_LINE_STRIP, 2, GL_UNSIGNED_INT, ((GLubyte *)NULL + (0)));
}
