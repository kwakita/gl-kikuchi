#ifndef VBOLINE_H
#define VBOLINE_H

class VBOLine
{

private:
    unsigned int vaoHandle;

public:
    VBOLine(float, float, float, float, float, float);

    void render();
};

#endif // VBOLINE_H
