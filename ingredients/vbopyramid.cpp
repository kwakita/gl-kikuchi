#include "vbopyramid.h"
#include "defines.h"

#include "cookbookogl.h"
#include "glutils.h"

#include <cstdio>

VBOPyramid::VBOPyramid()
{

    float v[4*3] = {
        0.0f, -0.33f, 0.94f,
        0.75f, -0.33f, -0.471f,
       -0.75f, -0.33f, -0.471f,
        0.0f, 1.0f, 0.0f
    };

    float n[4*3] = {
      2.24f, 1.0f, 1.41f,
      -2.24f, 1.0f, 1.41f, 
      0.0f, 1.0f, -2.83f,
      0.0f, -1.0f, 0.0f
    };

    float tex[16*2] = {
        // Front
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        // Right
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        // Back
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        // Left
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
    };

    GLuint el[] = {
        0,3,1,0,2,3,
        1,3,2,0,1,2
    };

    glGenVertexArrays( 1, &vaoHandle );
    glBindVertexArray(vaoHandle);

    unsigned int handle[4];
    glGenBuffers(4, handle);

    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(float), v, GL_STATIC_DRAW);
    glVertexAttribPointer( (GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, ((GLubyte *)NULL + (0)) );
    glEnableVertexAttribArray(0);  // Vertex position

    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(float), n, GL_STATIC_DRAW);
    glVertexAttribPointer( (GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, ((GLubyte *)NULL + (0)) );
    glEnableVertexAttribArray(1);  // Vertex normal

    glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
    glBufferData(GL_ARRAY_BUFFER, 16 * 2 * sizeof(float), tex, GL_STATIC_DRAW);
    glVertexAttribPointer( (GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, ((GLubyte *)NULL + (0)) );
    glEnableVertexAttribArray(2);  // texture coords

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 12 * sizeof(GLuint), el, GL_STATIC_DRAW);

    glBindVertexArray(0);
}

void VBOPyramid::render() {
    glBindVertexArray(vaoHandle);
    glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, ((GLubyte *)NULL + (0)));
}
