#ifndef VBOPYRAMID_H
#define VBOPYRAMID_H

class VBOPyramid
{

private:
    unsigned int vaoHandle;

public:
    VBOPyramid();

    void render();
};

#endif // VBOPYRAMID_H
