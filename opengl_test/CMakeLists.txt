set( opengl_test_SOURCES 
  main.cpp
  drawSphereWithInstance.cpp
  drawSphereWithVertexShader.cpp
  drawSphereWithComputeShader.cpp
  drawSphereWithRandomCoord.cpp
  drawSphereWithNoInstance.cpp
  drawPyramid.cpp
  triangleTess.cpp
  pointTess.cpp
  point.cpp
  pointCompute.cpp
  clickObject.cpp
  )

add_executable( opengl_test ${opengl_test_SOURCES} )
target_link_libraries( opengl_test ${GLSLCOOKBOOK_LIBS} )

#file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/shader DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
