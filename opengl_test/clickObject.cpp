#include "clickObject.h"

#include <cstdio>
#include <cstdlib>

#include <iostream>
using std::rand;
using std::cerr;
using std::endl;
#include <fstream>
using std::ifstream;
#include <sstream>
#include <string>
using std::string;
#include <vector>

#include "glutils.h"
using namespace std;

ClickObject::ClickObject() { }

const int pointnum = 100;
GLuint positionBufferHandle;
GLuint colorBufferHandle;
const int WIDTH = 600;
const int HEIGHT = 400; 

float positionData[pointnum * 6];
float colorData[pointnum * 3];
int id = -1;
int targetId = -1;

typedef pair<int, int> int_pair;
vector<int_pair> mainVec;

void importData() {
  string str;
  ifstream csvFile("./data.csv");
  while(getline(csvFile, str)) {
    stringstream ss(str);
    string item;
    int num;
    vector<int> vec;
    while (getline(ss, item, ',')) {
      if(!item.empty()) {
        istringstream is(item);
        is >> num;
        vec.push_back(num);
      }
    }
    if(vec.size() > 1) {
      mainVec.push_back(int_pair(vec.at(0), vec.at(1)));
    }
  }
}

void ClickObject::initScene(int a, int b, int c, int d)
{
  compileAndLinkShader();
  prog.setUniform("ClickedPosition", 0.0f, 0.0f, 0.0f);
  prog.setUniform("PrimitiveType" ,0);
  prog.setUniform("TargetId", targetId);

  /////////////////// Create the VBO ////////////////////
  for(int i = 0; i < pointnum * 2; i++) {
    positionData[i] = (((float)rand() / RAND_MAX) - 0.5) * 2;
  }
  for(int i = 0; i < pointnum * 3; i++) {
    colorData[i] = 1.0f;
  }

  importData();
  // Edgeを張るための2頂点をランダムで決定
  for(int i = 0; i < mainVec.size(); i++) {
    int num_1 = mainVec.at(i).first;
    int num_2 = mainVec.at(i).second;
    positionData[pointnum * 2 + 4*i] = positionData[2 * num_1];
    positionData[pointnum * 2 + 4*i + 1] = positionData[2 * num_1 + 1];
    positionData[pointnum * 2 + 4*i + 2] = positionData[2 * num_2];
    positionData[pointnum * 2 + 4*i + 3] = positionData[2 * num_2 + 1];
  }

  // Create and populate the buffer objects
  GLuint vboHandles[2];
  glGenBuffers(2, vboHandles);
  positionBufferHandle = vboHandles[0];
  colorBufferHandle = vboHandles[1];

  glBindBuffer(GL_ARRAY_BUFFER, positionBufferHandle);
  glBufferData(GL_ARRAY_BUFFER, pointnum * 6 * sizeof(float), positionData, GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, colorBufferHandle);
  glBufferData(GL_ARRAY_BUFFER, pointnum * 3 * sizeof(float), colorData, GL_STATIC_DRAW);

  // Create and set-up the vertex array object
  glGenVertexArrays( 1, &vaoHandle );
  glBindVertexArray(vaoHandle);

  glEnableVertexAttribArray(0);  // Vertex position
  glEnableVertexAttribArray(1);  // Vertex color

  glBindBuffer(GL_ARRAY_BUFFER, positionBufferHandle);
  glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );

  glBindBuffer(GL_ARRAY_BUFFER, colorBufferHandle);
  glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );
}


void ClickObject::update( float t )
{

}

void ClickObject::render(int a, int b, int c, int dom, int xpos, int ypos)
{
  glClear(GL_COLOR_BUFFER_BIT);

  prog.setUniform("ClickedPosition", xpos, HEIGHT - ypos, 0.0f);
  prog.setUniform("TargetId", targetId);

  // 座標の更新
  if(0 <=targetId && targetId < 100) {
          positionData[2*targetId] = ((2 * (float)xpos)/(float)WIDTH) - 1;
    positionData[2*targetId + 1] = 1 - ((2 * (float)ypos)/(float)HEIGHT);
for(int i = 0; i < mainVec.size(); i++) {
    int num_1 = mainVec.at(i).first;
    int num_2 = mainVec.at(i).second;
    positionData[pointnum * 2 + 4*i] = positionData[2 * num_1];
    positionData[pointnum * 2 + 4*i + 1] = positionData[2 * num_1 + 1];
    positionData[pointnum * 2 + 4*i + 2] = positionData[2 * num_2];
    positionData[pointnum * 2 + 4*i + 3] = positionData[2 * num_2 + 1];
  }

glBindBuffer(GL_ARRAY_BUFFER, positionBufferHandle);
  glBufferData(GL_ARRAY_BUFFER, pointnum * 6 * sizeof(float), positionData, GL_STATIC_DRAW);

  }
  glBindBuffer(GL_ARRAY_BUFFER, colorBufferHandle);
  glBufferData(GL_ARRAY_BUFFER, pointnum * 3 * sizeof(float), colorData, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, colorBufferHandle);
  glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );
  float *data;
  glBindVertexArray(vaoHandle);
  glPointSize(10);
   prog.setUniform("PrimitiveType", 1);
  glDrawArrays(GL_LINES, 100, 200);
 prog.setUniform("PrimitiveType", 0);
  glDrawArrays(GL_POINTS, 0, 100);

  float *colorData = new float[4];
  glReadPixels(xpos, HEIGHT - ypos, 1, 1, GL_RGBA, GL_FLOAT, colorData);
  if(colorData[0] * 255 >= 0 && colorData[0] * 255 < 100) {
    targetId = colorData[0] * 255;
  }
}

void ClickObject::resize(int w, int h)
{
    glViewport(0,0,w,h);
}

void ClickObject::compileAndLinkShader()
{
  try {
    prog.compileShader("shader/clickObject.vs");
    prog.compileShader("shader/clickObject.fs");
    prog.link();
    prog.validate();
    prog.use();
  } catch(GLSLProgramException & e) {
    cerr << e.what() << endl;
    exit( EXIT_FAILURE );
  }
}
