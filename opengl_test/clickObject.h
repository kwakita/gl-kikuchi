#ifndef CLICKOBJECT_H
#define CLICKOBJECT_H

#include "scene.h"
#include "glslprogram.h"
#include "vbopyramid.h"

#include "cookbookogl.h"

#include <glm/glm.hpp>
using glm::mat4;

class ClickObject : public Scene
{
private:
  GLSLProgram prog;

    int width, height;
    GLuint positionBufferHandle;
    GLuint colorBufferHandle;
    GLuint vboHandles[2];
    GLuint vaoHandle;

    void compileAndLinkShader();

public:
    ClickObject();

    void initScene(int, int, int, int);
    void update( float t );
    void render(int, int, int, int, int, int);
    void resize(int, int);
};

#endif // CLICKOBJECT_H
