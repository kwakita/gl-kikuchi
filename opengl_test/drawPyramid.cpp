#include "drawPyramid.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
using std::cerr;
using std::endl;

#include "glutils.h"
#include "defines.h"

using glm::vec3;

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

DrawPyramid::DrawPyramid() :angle(0.0f), tPrev(0.0f), rotSpeed(PI/8.0){
}

void DrawPyramid::initScene(int a, int b, int c, int d)
{
    compileAndLinkShader();

    glEnable(GL_DEPTH_TEST);
  angle = (float)(PI / 3.0);

    pyramid = new VBOPyramid();

    model = mat4(1.0f);
    model *= glm::rotate(glm::radians(-35.0f), vec3(1.0f,0.0f,0.0f));
    model *= glm::rotate(glm::radians(35.0f), vec3(0.0f,1.0f,0.0f));
    view = glm::lookAt(vec3(0.0f,0.0f,2.0f), vec3(0.0f,0.0f,0.0f), vec3(0.0f,1.0f,0.0f));
    projection = mat4(1.0f);

    prog.setUniform("Kd", 0.9f, 0.5f, 0.3f);
    prog.setUniform("Ld", 1.0f, 1.0f, 1.0f);
    prog.setUniform("LightPosition", view * vec4(5.0f,5.0f,2.0f,1.0f) );

}

void DrawPyramid::update( float t )
{
	float deltaT = t - tPrev;
	if(tPrev == 0.0f) deltaT = 0.0f;
	tPrev = t;

    angle += rotSpeed * deltaT;
    if( angle > TWOPI_F) angle -= TWOPI_F;

}

void DrawPyramid::render(int a, int b, int c, int d, int e, int f)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vec3 cameraPos(4.25f * cos(angle),3.0f,4.25f * sin(angle));
    view = glm::lookAt(cameraPos,
                       vec3(0.0f,0.0f,0.0f),
                       vec3(0.0f,1.0f,0.0f));

    model = mat4(1.0f);
    model = glm::translate(model, vec3(0.0f,-1.5f,0.0f));
    model = glm::rotate(model,glm::radians(-90.0f), vec3(1.0f,0.0f,0.0f));
    setMatrices();
    pyramid->render();
    glFinish();
}

void DrawPyramid::setMatrices()
{
    mat4 mv = view * model;
    prog.setUniform("ModelViewMatrix", mv);
    prog.setUniform("NormalMatrix",
                    mat3( vec3(mv[0]), vec3(mv[1]), vec3(mv[2]) ));
    prog.setUniform("MVP", projection * mv);
}

void DrawPyramid::resize(int w, int h)
{
    glViewport(0,0,w,h);
    width = w;
    height = h;
    projection = glm::perspective(glm::radians(70.0f), (float)w/h, 0.3f, 100.0f);
}

void DrawPyramid::compileAndLinkShader()
{
	try {
    	prog.compileShader("shader/drawPyramid.vert");
    	prog.compileShader("shader/drawPyramid.frag");
    	prog.link();
    	prog.validate();
    	prog.use();
    } catch(GLSLProgramException & e) {
 		cerr << e.what() << endl;
 		exit( EXIT_FAILURE );
    }
}
