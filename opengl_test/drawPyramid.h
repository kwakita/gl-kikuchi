#ifndef DRAWPYRAMID_H
#define DRAWPYRAMID_H

#include "scene.h"
#include "glslprogram.h"
#include "vbopyramid.h"

#include "cookbookogl.h"

#include <glm/glm.hpp>
using glm::mat4;

class DrawPyramid : public Scene
{
private:
    GLSLProgram prog;

    int width, height;
    float angle, tPrev, rotSpeed;
    VBOPyramid *pyramid;

    mat4 model;
    mat4 view;
    mat4 projection;

    void setMatrices();
    void compileAndLinkShader();

public:
    DrawPyramid();

    void initScene(int, int, int, int);
    void update( float t );
    void render(int, int, int, int, int, int);
    void resize(int, int);
};

#endif // DRAWPYRAMID_H
