#include "drawSphereWithInstance.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
using std::endl;
using std::cerr;

#include "glutils.h"
#include "defines.h"

using glm::vec3;

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

const int ARRAY_LENGTH = MAX_NUM * MAX_NUM * MAX_NUM * 3;
// 扱う頂点データを保存する配列
GLfloat *data = new GLfloat[ARRAY_LENGTH];

DrawSphereWithInstance::DrawSphereWithInstance() {}

void DrawSphereWithInstance::initScene(int num, int side, int height, int depth)
{
    compileAndLinkShader();

    glClearColor(0.5f,0.5f,0.5f,1.0f);

    glEnable(GL_DEPTH_TEST);

    sphere = new VBOSphere(Rad, TESS, TESS);
    
    ///////////////// Uniforms ///////////////
    angle = (float)(PI / 2.0f);
    //model = mat4(1.0f);
    view = glm::lookAt(vec3(3.0f * cos(angle),1.5f,3.0f * sin(angle)), vec3(0.0f,1.5f,0.0f), vec3(0.0f,1.0f,0.0f));
    //////////////////////////////////////////
    
    initBuffers(num, side, height, depth);

   }


void setDataToBuffer(int num, int side, int height, int depth, GLuint initVel) {
  vec3 v(0.0f);
  int size = num * num * num * 3 * sizeof(float);
  for( int i = 0; i < num; i++ ) {
    v.x = (i * 3)/100.0f + side * 0.01f;
    for(int j = 0; j < num; j++) {
      v.y = (j * 3)/100.0f + height * 0.01f;
      for(int k = 0; k < num; k++) {
        v.z = -((k * 3)/100.0f) + depth * 0.01f;
        data[3 * (num * num * i + num * j + k)]   = v.x;
        data[3 * (num * num * i + num * j + k)+1] = v.y;
        data[3 * (num * num * i + num * j + k)+2] = v.z;
      }
    }
  }
  glBindBuffer(GL_ARRAY_BUFFER,initVel);
  glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);
}

void initSetDataToBuffer(int num, int side, int height, int depth, GLuint initVel) {
  // data配列の初期化
  for(int  i = 0; i < sizeof data; i++) {
    data[i] = 0.0f;
  }
  int size = ARRAY_LENGTH * sizeof(float);
  glBindBuffer(GL_ARRAY_BUFFER, initVel);
  glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_STREAM_DRAW);
  setDataToBuffer(num, side, height, depth, initVel);
}


void DrawSphereWithInstance::initBuffers(int num, int side, int height, int depth)
{
    // Generate the buffers
    glGenBuffers(1, &initVel);   // Initial velocity buffer

    initSetDataToBuffer(num, side, height, depth, initVel);
    
    // Attach these to the torus's vertex array
    glBindVertexArray(sphere->getVertexArrayHandle());
    glBindBuffer(GL_ARRAY_BUFFER, initVel);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(2);

    glVertexAttribDivisor(2, 1);

    glBindVertexArray(2);
}

void DrawSphereWithInstance::update( float t )
{
}

void DrawSphereWithInstance::render(int num, int side, int height, int depth, int xpos, int ypos)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    setMatrices();
    setDataToBuffer(num, side, height, depth, initVel);
    
    glBindVertexArray(sphere->getVertexArrayHandle());
    glDrawElementsInstanced(GL_TRIANGLES, 6 * TESS * TESS, GL_UNSIGNED_INT, 0, num * num * num);
}

void DrawSphereWithInstance::setMatrices()
{
  mat4 mv = view * model;
  prog.setUniform("ModelViewMatrix", mv);
  prog.setUniform("NormalMatrix",
      mat3( vec3(mv[0]), vec3(mv[1]), vec3(mv[2]) ));
  prog.setUniform("ProjectionMatrix", projection);
}

void DrawSphereWithInstance::resize(int w, int h)
{
    glViewport(0,0,w,h);
    width = w;
    height = h;
    projection = glm::perspective(glm::radians(60.0f), (float)w/h, 0.3f, 100.0f);
}

void DrawSphereWithInstance::compileAndLinkShader()
{
  try {
    prog.compileShader("shader/drawSphereWithInstance.vs",GLSLShader::VERTEX);
    prog.compileShader("shader/drawSphereWithInstance.fs",GLSLShader::FRAGMENT);

    prog.link();
    prog.use();
  } catch(GLSLProgramException &e ) {
    cerr << e.what() << endl;
    exit( EXIT_FAILURE );
  }
}
