#ifndef SCENEPARTICLESINSTANCED_H
#define SCENEPARTICLESINSTANCED_H

#include "scene.h"
#include "glslprogram.h"
#include "vbosphere.h"

#include "cookbookogl.h"

#include <glm/glm.hpp>
using glm::mat4;

class DrawSphereWithInstance : public Scene
{
private:
    GLSLProgram prog;

    int width, height;

    GLuint initVel, startTime;

    int num;
    VBOSphere *sphere;

    mat4 model;
    mat4 view;
    mat4 projection;
    float angle;

    void setMatrices();
    void compileAndLinkShader();
    void initBuffers(int, int, int, int);

public:
    DrawSphereWithInstance();

    void initScene(int, int, int, int);
    void update( float t );
    void render(int, int, int, int, int, int);
    void resize(int, int);
};

#endif // SCENEPARTICLESINSTANCED_H
