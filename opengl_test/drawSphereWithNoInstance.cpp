#include "drawSphereWithNoInstance.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
using std::cerr;
using std::endl;

#include "glutils.h"
#include "defines.h"

using glm::vec3;

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

DrawSphereWithNoInstance::DrawSphereWithNoInstance() { }

void DrawSphereWithNoInstance::initScene(int a, int b, int c, int d)
{
    compileAndLinkShader();

    glClearColor(0.5f,0.5f,0.5f,1.0f);

    glEnable(GL_DEPTH_TEST);

    sphere = new VBOSphere(Rad, TESS, TESS);

    ///////////// Uniforms ////////////////////
    angle = (float)(PI / 2.0f);
    view = glm::lookAt(vec3(3.0f * cos(angle), 1.5f, 3.0f * sin(angle)), vec3(0.0f, 1.5f, 0.0f), vec3(0.0f, 1.0f, 0.0f));
    /////////////////////////////////////////////

    glPatchParameteri(GL_PATCH_VERTICES, 16);
}


void DrawSphereWithNoInstance::update( float t ) {}

void DrawSphereWithNoInstance::render(int num, int side, int height, int depth, int xpos, int ypos)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  for(int i = 0 ; i < num; i++) {
    for(int j = 0; j < num; j++) {
      for(int k = 0; k < num; k++) {
        model = glm::translate(mat4(1.0f), vec3((i * 3)/100.0f, (j * 3)/100.0f, -(k * 3)/100.0f) + vec3(side * 0.01f, height * 0.01f, depth * 0.01f));
        setMatrices();
        sphere->render();
      }
    }
  }
  glFinish();
}

void DrawSphereWithNoInstance::setMatrices()
{
    mat4 mv = view * model;
    prog.setUniform("ModelViewMatrix", mv);
    prog.setUniform("NormalMatrix",
                    mat3( vec3(mv[0]), vec3(mv[1]), vec3(mv[2]) ));
    prog.setUniform("ProjectionMatrix", projection);
}

void DrawSphereWithNoInstance::resize(int w, int h)
{
    glViewport(0,0,w,h);
    width = w;
    height = h;
    projection = glm::perspective(glm::radians(60.0f), (float)w/h, 0.3f, 30.0f);
}

void DrawSphereWithNoInstance::compileAndLinkShader()
{
  try {
    prog.compileShader("shader/drawSphereWithNoInstance.vs",GLSLShader::VERTEX);
    prog.compileShader("shader/drawSphereWithInstance.fs",GLSLShader::FRAGMENT);
    prog.link();
    prog.use();
  } catch(GLSLProgramException &e ) {
    cerr << e.what() << endl;
    exit( EXIT_FAILURE );
  }
}
