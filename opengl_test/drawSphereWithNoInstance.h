#ifndef DRAWSPHEREWITHNOINSTANCE_H
#define DRAWSPHEREWITHNOINSTANCE_H

#include "scene.h"
#include "glslprogram.h"
#include "vboteapotpatch.h"
#include "vbosphere.h"

#include "cookbookogl.h"

#include <glm/glm.hpp>
using glm::mat4;

class DrawSphereWithNoInstance : public Scene
{
private:
    GLSLProgram prog;
    
    int width, height;

    GLuint vaoHandle;
    VBOSphere *sphere;

    mat4 model;
    mat4 view, viewport;
    mat4 projection;
    float angle;

    void setMatrices();
    void compileAndLinkShader();

public:
    DrawSphereWithNoInstance();

    void initScene(int, int, int, int);
    void update( float t );
    void render(int, int, int, int, int, int);
    void resize(int, int);
};

#endif // DRAWSPHEREWITHNOINSTANCE_H
