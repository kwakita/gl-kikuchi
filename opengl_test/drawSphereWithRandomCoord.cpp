#include "drawSphereWithRandomCoord.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
using std::rand;

using std::cerr;
using std::endl;

#include "glutils.h"
#include "defines.h"

using glm::vec3;

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

DrawSphereWithRandomCoord::DrawSphereWithRandomCoord() { }

GLfloat coord[100000];

void DrawSphereWithRandomCoord::initScene(int num, int b, int c, int d)
{
  compileAndLinkShader();

  glClearColor(0.5f,0.5f,0.5f,1.0f);

  glEnable(GL_DEPTH_TEST);
  sphere = new VBOSphere(0.02f, 20, 20);
  for(int i = 0; i < 100000; i++) {
    coord[i] = 0.0f;
  }
  for(int i = 0 ; i < num; i++) {
    for(int j = 0; j < num; j++) {
      for(int k = 0; k < num; k++) {

        float xpos = ((float)rand() / RAND_MAX) * 4.0f - 2.0f;
        float ypos = ((float)rand() / RAND_MAX) * 4.0f - 2.0f;
        float zpos = ((float)rand() / RAND_MAX) * 4.0f + 1.0f;

        coord[(num*num*i + num*j + k) * 3] = xpos;
        coord[(num*num*i + num*j + k) * 3 + 1] = ypos;
        coord[(num*num*i + num*j + k) * 3 + 2] = zpos;
      }
    }
  }

  ///////////// Uniforms ////////////////////
  prog.setUniform("LightPosition", vec4(0.0f,0.0f,0.0f,1.0f));
  prog.setUniform("LightIntensity", vec3(1.0f,1.0f,1.0f));
  prog.setUniform("Kd", vec3(0.9f, 0.9f, 1.0f));
  prog.setUniform("Ld", 1.0f, 1.0f, 1.0f);
  /////////////////////////////////////////////

  glPatchParameteri(GL_PATCH_VERTICES, 16);
}


void DrawSphereWithRandomCoord::update( float t ) {}

void DrawSphereWithRandomCoord::render(int num, int b, int c, int d, int e, int f)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  vec3 cameraPos(0.0f ,1.0f,6.25f);
  view = glm::lookAt(cameraPos,
      vec3(0.0f,0.0f,0.0f),
      vec3(0.0f,1.0f,0.0f));
  for(int i = 0 ; i < num; i++) {
    for(int j = 0; j < num; j++) {
      for(int k = 0; k < num; k++) {
        model = mat4(1.0f);
        // 球のx座標(中心を中央にするように計算)
        //float xpos = (num/2) * (-0.05f) + i * 0.05f;
        // 球のy座標
        //float ypos = 1.0f + j * 0.05f;
        // 球のz座標
        //float zpos = k * 0.05f;

        float xpos = coord[(num*num*i + num*j + k) * 3];
        float ypos = coord[(num*num*i + num*j + k) * 3 + 1];
        float zpos = coord[(num*num*i + num*j + k) * 3 + 2];

        model = glm::translate(model, vec3(xpos, ypos, zpos));
        model = glm::translate(model, vec3(0.0f,0.0f,0.0f));
        model = glm::rotate(model,glm::radians(-90.0f), vec3(1.0f,0.0f,0.0f));
        setMatrices();
        sphere->render();
      }
    }
  }
  for(int i = 0; i < num * 10; i++) {
    model = mat4(1.0f);
    line = new VBOLine(coord[6 * i], coord[6 * i + 1], coord[6 * i + 2], 
        coord[6 * i + 3], coord[6 * i + 4], coord[6 * i + 5]);
    setMatrices();
    line->render();
  }

  glFinish();
}

void DrawSphereWithRandomCoord::setMatrices()
{
    mat4 mv = view * model;
  prog.setUniform("ModelViewMatrix", mv);
  prog.setUniform("NormalMatrix",
      mat3( vec3(mv[0]), vec3(mv[1]), vec3(mv[2]) ));
  prog.setUniform("ProjectionMatrix", projection);
  prog.setUniform("MVP", projection * mv);
  prog.setUniform("ViewportMatrix", viewport);
}

void DrawSphereWithRandomCoord::resize(int w, int h)
{
    glViewport(0,0,w,h);

    float w2 = w / 2.0f;
    float h2 = h / 2.0f;
    viewport = mat4( vec4(w2,0.0f,0.0f,0.0f),
                     vec4(0.0f,h2,0.0f,0.0f),
                     vec4(0.0f,0.0f,1.0f,0.0f),
                     vec4(w2+0, h2+0, 0.0f, 1.0f));
    projection = glm::perspective(glm::radians(60.0f), (float)w/h, 0.3f, 30.0f);
}

void DrawSphereWithRandomCoord::compileAndLinkShader()
{
	try {
		prog.compileShader("shader/drawSphereWithRandomCoord.vs",GLSLShader::VERTEX);
		prog.compileShader("shader/drawSphereWithRandomCoord.fs",GLSLShader::FRAGMENT);
	    	prog.link();
    	prog.use();
    } catch(GLSLProgramException &e ) {
    	cerr << e.what() << endl;
 		exit( EXIT_FAILURE );
    }
}
