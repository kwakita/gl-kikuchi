#ifndef DRAWSPHEREWITHRANDOMCOORD_H
#define DRAWSPHEREWITHRANDOMCOORD_H

#include "scene.h"
#include "glslprogram.h"
#include "vboteapotpatch.h"
#include "vbosphere.h"
#include "vboline.h"

#include "cookbookogl.h"

#include <glm/glm.hpp>
using glm::mat4;

class DrawSphereWithRandomCoord : public Scene
{
private:
    GLSLProgram prog;

    GLuint vaoHandle;
    VBOSphere *sphere;
    VBOLine *line;

    mat4 model;
    mat4 view, viewport;
    mat4 projection;

    void setMatrices();
    void compileAndLinkShader();

public:
    DrawSphereWithRandomCoord();

    void initScene(int, int, int, int);
    void update( float t );
    void render(int, int, int, int, int, int);
    void resize(int, int);
};

#endif // DRAWSPHEREWITHRANDOMCOORD_H
