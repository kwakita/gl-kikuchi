#include "drawSphereWithVertexShader.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
using std::endl;
using std::cerr;

#include "glutils.h"
#include "defines.h"

using glm::vec3;

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

DrawSphereWithVertexShader::DrawSphereWithVertexShader(){}

// 初期化
void DrawSphereWithVertexShader::initScene(int num, int side, int height, int depth)
{
  compileAndLinkShader();

  glClearColor(0.5f,0.5f,0.5f,1.0f);

  glEnable(GL_DEPTH_TEST);

  sphere = new VBOSphere(Rad, TESS, TESS);
  projection = mat4(1.0f);

  angle = (float)(PI / 2.0f);
  model = mat4(1.0f);
}

void DrawSphereWithVertexShader::initBuffers(int num, int side, int height, int depth) {}


void DrawSphereWithVertexShader::update( float t ) {}

void DrawSphereWithVertexShader::render(int num, int side, int height, int depth, int xpos, int ypos)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   prog.setUniform("Num", vec4(num, side, height, depth));
  setMatrices();
  glBindVertexArray(sphere->getVertexArrayHandle());
  glDrawElementsInstanced(GL_TRIANGLES, 6 * 10 * 10, GL_UNSIGNED_INT, 0, num * num * num);
}

void DrawSphereWithVertexShader::setMatrices()
{
  view = glm::lookAt(vec3(3.0f * cos(angle),1.5f,3.0f * sin(angle)), vec3(0.0f,1.5f,0.0f), vec3(0.0f,1.0f,0.0f));
     model = mat4(1.0f);

  mat4 mv = view * model;
  prog.setUniform("ModelViewMatrix", mv);
  prog.setUniform("NormalMatrix",
      mat3( vec3(mv[0]), vec3(mv[1]), vec3(mv[2]) ));
  prog.setUniform("ProjectionMatrix", projection);
}

void DrawSphereWithVertexShader::resize(int w, int h)
{
  glViewport(0,0,w,h);
  width = w;
  height = h;
  projection = glm::perspective(glm::radians(60.0f), (float)w/h, 0.3f, 100.0f);
}

void DrawSphereWithVertexShader::compileAndLinkShader()
{
  try {
    prog.compileShader("shader/drawSphereWithVertexShader.vs",GLSLShader::VERTEX);
    prog.compileShader("shader/drawSphereWithInstance.fs",GLSLShader::FRAGMENT);

    prog.link();
    prog.use();
  } catch(GLSLProgramException &e ) {
    cerr << e.what() << endl;
    exit( EXIT_FAILURE );
  }
}
