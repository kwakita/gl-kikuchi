#include "cookbookogl.h"
#include <GLFW/glfw3.h>

#include <cstdio>
#include <cstdlib>
#include <sstream>

#include "glutils.h"
#include "drawSphereWithInstance.h"
#include "drawSphereWithVertexShader.h"
#include "drawSphereWithComputeShader.h"
#include "drawSphereWithNoInstance.h"
#include "drawSphereWithRandomCoord.h"
#include "drawPyramid.h"
#include "triangleTess.h"
#include "pointTess.h"
#include "point.h"
#include "pointCompute.h"
#include "clickObject.h"
#include "sceneparticles.h"
#define WIN_WIDTH 600
#define WIN_HEIGHT 400

Scene *scene;
GLFWwindow *window;

string parseCLArgs(int argc, char ** argv);
void printHelpInfo(const char *);

// 格子の初期値
int num = 10;
// 左右座標の移動の初期値
int side = 0;
// 上下座標の移動の初期値
int height = 0;
// 前後座標の移動の初期値
int depth = 0;
// マウスポインタのx座標
double cursorXPos = 0;
// マウスポインタのy座標
double cursorYPos = 0;
// 直近でクリックされた位置のx座標
double clickedXPos = 0;
// 直近でクリックされた位置のy座標
double clickedYPos = 0;

void initializeGL() {
  glDebugMessageCallback(GLUtils::debugCallback, NULL);
  glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
  glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER, 0,
  GL_DEBUG_SEVERITY_NOTIFICATION, -1 , "Start debugging");

    glClearColor(0.5f,0.5f,0.5f,1.0f);
    scene->initScene(num, side, height, depth);
}
const double MeasurementTime = 0.5;
int renderedFrames = 0;
double dueTime = -1;

static void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos) {
  cursorXPos = xpos;
  cursorYPos = ypos;
}

// キーボードやマウスによる操作を取得
void getKeyControll() {
  bool state = glfwGetKey(window, GLFW_KEY_PAGE_UP);
  if(glfwGetKey(window, GLFW_KEY_PAGE_UP)) {
    depth++;
  }
  if(glfwGetKey(window, GLFW_KEY_PAGE_DOWN)) {
    depth--;
  }
  if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)) {
    num++;
    if(num > 100) {
      num = 100;
    }
    clickedXPos = cursorXPos;
    clickedYPos = cursorYPos;
  }
  if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT)) {
    num--;
    if(num < 1) {
      num = 1;
    }
  }
  if(glfwGetKey(window, GLFW_KEY_UP)) {
    height++;
  }
  if(glfwGetKey(window, GLFW_KEY_DOWN)) {
    height--;
  }
  if(glfwGetKey(window, GLFW_KEY_RIGHT)) {
    side++;
  }
  if(glfwGetKey(window, GLFW_KEY_LEFT)) {
    side--;
  }

}

void mainLoop() {
  while( ! glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE) ) {
    getKeyControll();
    GLUtils::checkForOpenGLError(__FILE__,__LINE__);
    double t = glfwGetTime();
    scene->update(glfwGetTime());
    scene->render(num, side, height, depth, clickedXPos, clickedYPos);
    renderedFrames++;
    if (t > dueTime) {
      std::ostringstream s;
      s << "fps: " << (int)(renderedFrames / MeasurementTime) << ", n=" << num << ",xpos:" << clickedXPos << ",ypos:" << clickedYPos;
      glfwSetWindowTitle(window, s.str().c_str());


      renderedFrames = 0;
      dueTime = t + MeasurementTime;
    }
    glfwSwapBuffers(window);
    glfwPollEvents();
  }
}

void resizeGL(int w, int h ) {
    scene->resize(w,h);
}

int main(int argc, char *argv[])
{
  string recipe = parseCLArgs(argc, argv);

  // Initialize GLFW
  if( !glfwInit() ) exit( EXIT_FAILURE );

  // Select OpenGL 4.3 with a forward compatible core profile.
  glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
  glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

  // Open the window
  string title = "OpenGL TEST -- " + recipe;
  window = glfwCreateWindow( WIN_WIDTH, WIN_HEIGHT, title.c_str(), NULL, NULL);
  if( ! window ) {
    glfwTerminate();
    exit( EXIT_FAILURE );
  }
  glfwMakeContextCurrent(window);

  // Load the OpenGL functions.
  if( ogl_LoadFunctions() == ogl_LOAD_FAILED ) {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  GLUtils::dumpGLInfo();

  // Initialization
  initializeGL();
  resizeGL(WIN_WIDTH,WIN_HEIGHT);

  glfwSetCursorPosCallback(window, cursor_pos_callback);
  // Enter the main loop
  mainLoop();

  // Close window and terminate GLFW
  glfwTerminate();
  // Exit program
  exit( EXIT_SUCCESS );
}

string parseCLArgs(int argc, char ** argv) {

  if( argc < 2 ) {
    printHelpInfo(argv[0]);
    exit(EXIT_FAILURE);
  }

  string recipe = argv[1];

  if( recipe == "drawspherewithinstance" ) {
    scene = new DrawSphereWithInstance();
  }else if(recipe == "drawspherewithvs") {
    scene = new DrawSphereWithVertexShader();
  }else if(recipe == "drawspherewithcs") {
    scene = new DrawSphereWithComputeShader();
  }else if(recipe == "drawspherewithnoinstance") {
    scene = new DrawSphereWithNoInstance();
  }else if(recipe == "drawspherewithrandomcoord") {
    scene = new DrawSphereWithRandomCoord();
  }else if(recipe == "drawpyramid") {
    scene = new DrawPyramid();
  }else if(recipe == "triangletess") {
    scene = new TriangleTess();
  }else if(recipe == "pointtess") {
    scene = new PointTess();
  }else if(recipe == "point") {
    scene = new Point();
  }else if(recipe == "pointcompute") {
    scene = new PointCompute();
  }else if(recipe == "clickobject") {
    scene = new ClickObject();
//  }else if(recipe == "particles") {
//    scene = new SceneParticles();
  } else {
    printf("Unknown recipe: %s\n", recipe.c_str());
    printHelpInfo(argv[0]);
    exit(EXIT_FAILURE);
  }

  return recipe;
}

void printHelpInfo(const char * exeFile) {
  printf("Usage: %s recipe-name\n\n", exeFile);
  printf("Recipe names: \n");
  printf("  drawspherewithinstance  : draw sphere with instance\n");
  printf("  drawspherewithvs : draw sphere with vertex shader\n");
  printf("  drawspherewithcs : draw sphere with compute shader\n");
  printf("  drawspherewithnoinstance : draw sphere with no instance\n");
  printf("  drawspherewithrandomcoord : draw sphere with random coordinate\n");
  printf("  drawpyramid : draw pyramid\n");
  printf("  triangletess : draw triangle with tess\n");
   printf("  pointtess : draw point with tess\n");
   printf("  point: draw point\n");
   printf("  pointcompute: draw point with compute shader\n");
   printf("  clickobject : click object\n");
   printf("  particles\n");
  
}
