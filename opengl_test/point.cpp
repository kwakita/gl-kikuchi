#include "point.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
using std::rand;
using std::cerr;
using std::endl;

#include "glutils.h"
#include "defines.h"

using glm::vec3;

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

Point::Point() { }

int size = 2;

void Point::initScene(int num, int b, int c, int d)
{
  compileAndLinkShader();

  glClearColor(0.5f,0.5f,0.5f,1.0f);

  glEnable(GL_DEPTH_TEST);

  /////// Uniforms //////////
  angle = (float)(PI / 2.0f);
  view = glm::lookAt(vec3(3.0f * cos(angle),1.5f,3.0f * sin(angle)), vec3(0.0f,1.5f,0.0f), vec3(0.0f,1.0f,0.0f));

  // Set up patch VBO

  GLuint vboHandle;
  glGenBuffers(1, &vboHandle);
  int pointer;
  // generate point
  GLfloat *p = new GLfloat[num * num * num * 3];
  for(int i = 0; i < num; i++) {
    for(int j = 0; j < num; j++) {
      for(int k = 0; k < num; k++) {
        pointer = 3 * (num*num*i + num*j + k);
        p[pointer] = (i)/200.0f - 0.2;
        p[pointer + 1] = (j)/200.0f -0.1;
        p[pointer + 2] = -(k)/200.0f;
      }
    }
  }

  glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
  glBufferData(GL_ARRAY_BUFFER, num * num * num * 3 * sizeof(float), p, GL_STATIC_DRAW);


  // Set up patch VAO
  glGenVertexArrays(1, &vaoHandle);
  glBindVertexArray(vaoHandle);

  glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0);
  glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
}


void Point::update( float t ) {}

void Point::render(int num, int b, int c, int d, int e, int f)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    setMatrices();
    glBindVertexArray(vaoHandle);
    glPointSize(2);
    glDrawArrays(GL_PATCHES, 0, num * num * num * 3);

    glFinish();
}

void Point::setMatrices()
{
  mat4 mv = view * model;
  prog.setUniform("ModelViewMatrix", mv);
  prog.setUniform("ProjectionMatrix", projection);
  prog.setUniform("PointSize", size);
}

void Point::resize(int w, int h)
{
    glViewport(0,0,w,h);
    width = w;
    height = h;
    projection = glm::perspective(glm::radians(60.0f), (float)w/h, 0.3f, 100.0f);
}

void Point::compileAndLinkShader()
{
	try {
		prog.compileShader("shader/point.vs",GLSLShader::VERTEX);
		prog.compileShader("shader/point.fs",GLSLShader::FRAGMENT);
    	prog.link();
    	prog.use();
    } catch(GLSLProgramException &e ) {
    	cerr << e.what() << endl;
 		exit( EXIT_FAILURE );
    }
}
