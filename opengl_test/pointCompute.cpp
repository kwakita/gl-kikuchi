#include "pointCompute.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
using std::rand;
using std::cerr;
using std::endl;

#include "glutils.h"
#include "defines.h"

using glm::vec3;

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

PointCompute::PointCompute() { }

int pointSize = 2;

void PointCompute::initScene(int num, int b, int c, int d)
{
  compileAndLinkShader();

  glClearColor(0.5f,0.5f,0.5f,1.0f);

  glEnable(GL_DEPTH_TEST);

  /////// Uniforms //////////
  angle = (float)(PI / 2.0f);
  view = glm::lookAt(vec3(3.0f * cos(angle),1.5f,3.0f * sin(angle)), vec3(0.0f,1.5f,0.0f), vec3(0.0f,1.0f,0.0f));

  // Set up patch VBO

  GLuint vboHandle;
  glGenBuffers(1, &vboHandle);
  int pointer;
  // generate point
  vector<GLfloat> initPos;
  for( int i = 0; i < num; i++ ) {
    for( int j = 0; j < num; j++ ) {
      for( int k = 0; k < num; k++ ) {
        initPos.push_back(0.0);
        initPos.push_back(0.0);
        initPos.push_back(0.0);
      }
    }
  }

  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, vboHandle);
  glBufferData(GL_SHADER_STORAGE_BUFFER, num * num * num * 3 * sizeof(float), &initPos[0], GL_DYNAMIC_DRAW);


  // Set up patch VAO
  glGenVertexArrays(1, &vaoHandle);
  glBindVertexArray(vaoHandle);

  glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0);
  glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

}


void PointCompute::update( float t ) {}

void PointCompute::render(int num, int b, int c, int d, int e, int f)
{
  // Execute the compute shader
    computeProg.use();
    glDispatchCompute(30, 1, 1);
    glMemoryBarrier( GL_SHADER_STORAGE_BARRIER_BIT );
// Draw the scene
        renderProg.use();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    setMatrices();
    glBindVertexArray(vaoHandle);
    glPointSize(2);
    glDrawArrays(GL_PATCHES, 0, num * num * num * 3);
    glBindVertexArray(0);
}

void PointCompute::setMatrices()
{
  mat4 mv = view * model;
  renderProg.setUniform("ModelViewMatrix", mv);
  renderProg.setUniform("ProjectionMatrix", projection);
  renderProg.setUniform("PointSize", pointSize);
}

void PointCompute::resize(int w, int h)
{
    glViewport(0,0,w,h);
    width = w;
    height = h;
    projection = glm::perspective(glm::radians(60.0f), (float)w/h, 0.3f, 100.0f);
}

void PointCompute::compileAndLinkShader()
{
	try {
		renderProg.compileShader("shader/pointCompute.vs",GLSLShader::VERTEX);
		renderProg.compileShader("shader/pointCompute.fs",GLSLShader::FRAGMENT);
    	renderProg.link();
    	renderProg.use();

        computeProg.compileShader("shader/pointCompute.cs");
        computeProg.link();
    } catch(GLSLProgramException &e ) {
    	cerr << e.what() << endl;
 		exit( EXIT_FAILURE );
    }
}
