#ifndef POINTCOMPUTE_H
#define POINTCOMPUTE_H

#include "scene.h"
#include "glslprogram.h"
#include "vboplane.h"
#include "vbocube.h"
#include "vbotorus.h"
#include "vboteapot.h"
#include "vbomeshadj.h"

#include "cookbookogl.h"

#include <glm/glm.hpp>
using glm::mat4;

class PointCompute : public Scene
{
private:
    GLSLProgram renderProg, computeProg;

    int width, height;
    GLuint vaoHandle;

    GLuint particlesVao;
    GLuint bhVao, bhBuf;
    mat4 model;
    mat4 view;
    mat4 projection;
    float angle;

    void setMatrices();
    void compileAndLinkShader();

public:
    PointCompute();

    void initScene(int, int, int, int);
    void update( float t );
    void render(int, int, int, int, int, int);
    void resize(int, int);
};

#endif // POINTCOMPUTE_H
