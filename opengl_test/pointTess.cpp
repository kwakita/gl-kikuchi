#include "pointTess.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
using std::rand;
using std::cerr;
using std::endl;

#include "glutils.h"
#include "defines.h"

using glm::vec3;

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

PointTess::PointTess() { }

void PointTess::initScene(int num, int b, int c, int d)
{
  compileAndLinkShader();

  glClearColor(0.5f,0.5f,0.5f,1.0f);

  glEnable(GL_DEPTH_TEST);

  /////// Uniforms //////////
  angle = (float)(PI / 2.0f);
  view = glm::lookAt(vec3(3.0f * cos(angle),1.5f,3.0f * sin(angle)), vec3(0.0f,1.5f,0.0f), vec3(0.0f,1.0f,0.0f));

  // Set up patch VBO

  GLuint vboHandle;
  glGenBuffers(1, &vboHandle);
  int pointer;
  // generate point
  GLfloat *p = new GLfloat[num * num * num * 4];
  for(int i = 0; i < num; i++) {
    for(int j = 0; j < num; j++) {
      for(int k = 0; k < num; k++) {
        pointer = 4 * (num*num*i + num*j + k);
        p[pointer] = (i)/100.0f;
        p[pointer + 1] = (j)/100.0f;
        p[pointer + 2] = (k)/100.0f;
        p[pointer + 3] = Rad; 
      }
    }
  }

  glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
  glBufferData(GL_ARRAY_BUFFER, num * num * num * 4 * sizeof(float), p, GL_STATIC_DRAW);


  // Set up patch VAO
  glGenVertexArrays(1, &vaoHandle);
  glBindVertexArray(vaoHandle);

  glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0);

  // Set the number of vertices per patch.  IMPORTANT!!
  glPatchParameteri( GL_PATCH_VERTICES, 1);

  GLint maxVerts;
  glGetIntegerv(GL_MAX_PATCH_VERTICES, &maxVerts);
  printf("Max patch vertices: %d\n", maxVerts);
}


void PointTess::update( float t ) {}

void PointTess::render(int num, int b, int c, int d, int e ,int f)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    setMatrices();
    glBindVertexArray(vaoHandle);
    glDrawArrays(GL_PATCHES, 0, num * num * num * 4);

    glFinish();
}

void PointTess::setMatrices()
{
  mat4 mv = view * model;
  prog.setUniform("ModelViewMatrix", mv);
  prog.setUniform("NormalMatrix",
      mat3( vec3(mv[0]), vec3(mv[1]), vec3(mv[2]) ));
  prog.setUniform("ProjectionMatrix", projection);
  prog.setUniform("MVP", projection * mv);
}

void PointTess::resize(int w, int h)
{
    glViewport(0,0,w,h);
    width = w;
    height = h;
    projection = glm::perspective(glm::radians(60.0f), (float)w/h, 0.3f, 100.0f);
}

void PointTess::compileAndLinkShader()
{
	try {
		prog.compileShader("shader/pointTess.vs",GLSLShader::VERTEX);
		prog.compileShader("shader/drawSphereWithInstance.fs",GLSLShader::FRAGMENT);
		//prog.compileShader("shader/pointTess.gs",GLSLShader::GEOMETRY);
		prog.compileShader("shader/pointTess.tes",GLSLShader::TESS_EVALUATION);
		prog.compileShader("shader/pointTess.tcs",GLSLShader::TESS_CONTROL);
    	prog.link();
    	prog.use();
    } catch(GLSLProgramException &e ) {
    	cerr << e.what() << endl;
 		exit( EXIT_FAILURE );
    }
}
