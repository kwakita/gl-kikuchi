#version 430

in vec4 Color;
in int gl_PrimitiveID;
in vec4 gl_FragCoord;
uniform vec3 ClickedPosition;
uniform int TargetId;
layout (location=0) out vec4 FragColor;
const float S = 255.0f;

vec3 light = vec3(0.1, 0.3, 0.8);

void main() {
  if(Color.w == 0) {
    /*float distance = sqrt((gl_FragCoord.x - ClickedPosition.x)*(gl_FragCoord.x - ClickedPosition.x) + 
      (gl_FragCoord.y - ClickedPosition.y)*(gl_FragCoord.y - ClickedPosition.y));*/

    vec3 n;
    n.xy = gl_PointCoord * 2.0 - 1.0;
    n.z = 1.0 - dot(n.xy, n.xy);
    if(n.z < 0.0) discard;

    n.z = sqrt(n.z);
    vec3 m = normalize(n);
    float d = dot(light, m);

    float xDis = sqrt((gl_FragCoord.x - ClickedPosition.x)*(gl_FragCoord.x - ClickedPosition.x));
    float yDis = sqrt((gl_FragCoord.y - ClickedPosition.y)*(gl_FragCoord.y - ClickedPosition.y));

    if(xDis < 3.0 && yDis < 3.0) {
      FragColor = vec4(gl_PrimitiveID / S, gl_PrimitiveID / S, 0.0, 0.0);
    } else {
      if(gl_PrimitiveID == TargetId) {
        FragColor == vec4(1.0, 1.0, 0.0, 0.0);
      } else {
      FragColor = vec4(vec3(d), 0.0);
      }
    }
  } else {
    FragColor = vec4(0.0, 1.0, 0.0, 0.0);
  }
  /*  } else {
        FragColor = vec4(Color.xyz, 0.0);
        }*/
}


