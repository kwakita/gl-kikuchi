#version 430

layout (location=0) in vec2 VertexPosition;
layout (location=1) in vec3 VertexColor;
uniform vec3 ClickedPosition;
uniform int PrimitiveType;
out vec4 Color;
out int P_Type;

void main()
{
    Color = vec4(VertexColor, PrimitiveType);
        gl_Position = vec4(VertexPosition, 0.0, 1.0);
}
