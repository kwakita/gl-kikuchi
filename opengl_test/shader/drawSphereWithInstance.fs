#version 400

in vec3 Position;
in vec3 Normal;

layout ( location = 0 ) out vec4 FragColor;
  
vec3 phongModel( vec3 pos, vec3 norm )
{
    vec3 s = normalize(vec3(0.0) - pos);
    float sDotN = max( dot(s,norm), 0.0 );
    vec3 diffuse = vec3(1.0) * vec3(0.9, 0.5, 0.2) * sDotN;
    return diffuse;
}

void main()
{
    FragColor = vec4(phongModel(Position,Normal), 1.0);
}
