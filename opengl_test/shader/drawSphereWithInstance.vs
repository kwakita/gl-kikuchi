#version 400

layout (location = 0) in vec3 VertexPosition;
layout (location = 1) in vec3 VertexNormal;
layout (location = 2) in vec3 VertexVelocity;

out vec3 Position;
out vec3 Normal;

uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;

void main()
{
    Position = (ModelViewMatrix * vec4(VertexPosition + VertexVelocity,1.0)).xyz;
    Normal = NormalMatrix * VertexNormal;
    gl_Position = ProjectionMatrix * vec4(Position, 1.0); 
}
