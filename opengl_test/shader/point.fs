#version 400

layout (location=0) out vec4 FragColor;

vec3 light = vec3(0.1, 0.5, 0.8);

void main() {
  vec3 n;
  n.xy = gl_PointCoord * 2.0 - 1.0;
  n.z = 1.0 - dot(n.xy, n.xy);
  if(n.z < 0.0) discard;

  n.z = sqrt(n.z);
  vec3 m = normalize(n);
  float d = dot(light, m);

  FragColor = vec4(vec3(d), 1.0);
}
