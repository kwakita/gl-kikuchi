#version 430

layout( local_size_x = 100 ) in;

layout(std430, binding=0) buffer Pos {
  float Position[];
};

void main() {
  uint idx = gl_GlobalInvocationID.x;
  int id = int(idx)/3;
  int xpos = int(id/100);
  int ypos = int((id - xpos*100)/10);
  int zpos = int(id - xpos*100 - ypos*10);
  if(int(idx)%3 == 0) {
  Position[idx] = 0.01 * xpos -0.05;
  }else if(int(idx)%3 == 1) {
    Position[idx] = 0.01 * ypos + 0.1;
  }else {
    Position[idx] = 0.01 * zpos;
  }
}
