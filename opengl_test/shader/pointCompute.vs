#version 430

layout (location = 0 ) in vec3 VertexPosition;

uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;
uniform int PointSize;
void main()
{
  gl_Position = ProjectionMatrix * ModelViewMatrix * vec4(VertexPosition, 0.1);
  gl_PointSize = PointSize / gl_Position.w;
}
