#version 400

layout (location = 0 ) in vec4 VertexPosition;

void main()
{
  gl_Position = VertexPosition;
}
