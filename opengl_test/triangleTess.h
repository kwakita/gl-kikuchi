#ifndef TRIANGLETESS_H
#define TRIANGLETESS_H

#include "scene.h"
#include "glslprogram.h"
#include "vboplane.h"
#include "vbocube.h"
#include "vbotorus.h"
#include "vboteapot.h"
#include "vbomeshadj.h"

#include "cookbookogl.h"

#include <glm/glm.hpp>
using glm::mat4;

class TriangleTess : public Scene
{
private:
    GLSLProgram prog;

    GLuint vaoHandle;

    mat4 model;
    mat4 view, viewport;
    mat4 projection;
    float c;
    void setMatrices();
    void compileAndLinkShader();

public:
    TriangleTess();

    void initScene(int, int, int, int);
    void update( float t );
    void render(int, int, int, int, int, int);
    void resize(int, int);
};

#endif // TRIANGLETESS_H
